INSERT INTO TIPORECURSO (nome) VALUES ('Imagem');
INSERT INTO TIPORECURSO (nome) VALUES ('Som');
INSERT INTO TIPORECURSO (nome) VALUES ('Texto');
INSERT INTO TIPORECURSO (nome) VALUES ('E-Book');
INSERT INTO TIPORECURSO (nome) VALUES ('Vídeo');


INSERT INTO AUTOR (area_atuacao, email, telefone, nome_completo) VALUES ('Programação', 'jose.silva.br@outlook.com', '11952582091', 'José Antônio da Silva (1940327-5)');
INSERT INTO AUTOR (area_atuacao, email, telefone, nome_completo) VALUES ('Programação', 'micael.victor@outlook.com', '11900000000', 'Micael Victor de Barros (1882146-4)');
INSERT INTO AUTOR (area_atuacao, email, telefone, nome_completo) VALUES ('Programação', 'leonardo.rodrigues@outlook.com', '11900000000', 'Leonardo Rodrigues Evangelista (1888185-8)');
INSERT INTO AUTOR (area_atuacao, email, telefone, nome_completo) VALUES ('Programação', 'diego.apolinario@outlook.com', '11900000000', 'Diego Apolinário (1892295-3)');
INSERT INTO AUTOR (area_atuacao, email, telefone, nome_completo) VALUES ('Programação', 'larissa.alves@outlook.com', '11900000000', 'Larissa Alves Justi (1893134-1)');
INSERT INTO AUTOR (area_atuacao, email, telefone, nome_completo) VALUES ('Programação', 'alice@outlook.com', '11900000000', 'Alice Maria Santos Alencar (1954124-4)');
INSERT INTO AUTOR (area_atuacao, email, telefone, nome_completo) VALUES ('Programação', 'carlos@outlook.com', '11900000000', 'Carlos Petrosino (1933155-0)');


INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');
INSERT INTO RECURSODIDATICO 
(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) 
VALUES 
('Programação Funcional', 'Uma introdução em Clojure', 'Fundamental', 'Com ela, você pode produzir códigos mais robustos, menos suscetíveis a erros e expandir sua forma de pensar.', 'Técnicas de Programação', 'SERVIDOR', 'programacao_funcional.pdf', 4, 'Funcional, produzir, programacao_funcional');

INSERT INTO Atualizado (id_rd, id_autor, descricao) VALUE (1, 1, 'Atualização');
INSERT INTO Atualizado (id_rd, id_autor, descricao) VALUE (2, 4, 'Atualização');
INSERT INTO Atualizado (id_rd, id_autor, descricao) VALUE (3, 2, 'Atualização');
INSERT INTO Atualizado (id_rd, id_autor, descricao) VALUE (1, 2, 'Atualização');
INSERT INTO Atualizado (id_rd, id_autor, descricao) VALUE (5, 2, 'Atualização');
