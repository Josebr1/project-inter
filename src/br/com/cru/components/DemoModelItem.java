package br.com.cru.components;

public class DemoModelItem {
    public String objectName;
    public String objectType;
     
    public DemoModelItem(String objectName,String objectType){
        this.objectName = objectName;
        this.objectType = objectType;
    }
     
    public String toString(){
        return objectType+"/"+objectName;
    }
}
