package br.com.cru.dao;

import br.com.cru.interfaces.IAbstractDao;
import br.com.cru.model.Autor;
import br.com.cru.model.TipoRecurso;
import br.com.cru.utils.DbUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JOptionPane;

public class AutorDAO implements IAbstractDao<Autor>{

    private Connection connection = null;
    
    @Override
    public List<Autor> all() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        List<Autor> listaAutores = new ArrayList<>();
        ResultSet rs;
        
        try {
            connection = DbUtils.getConnection();
            
            String sql = "SELECT * FROM AUTOR";
            
            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            
            rs = statement.executeQuery();
            
            while (rs.next()) {
                listaAutores.add(new Autor(rs.getInt("id_autor"), rs.getString("nome_completo"), rs.getString("area_atuacao"), rs.getString("email"), rs.getString("telefone")));
            }
            
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        return listaAutores;
    }

    @Override
    public Autor findById(long id) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
       ResultSet resultSet;

        try {
            connection = DbUtils.getConnection();
            String sql = "SELECT * FROM AUTOR WHERE id_autor = ?";
            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);

            statement.setLong(1, id);

            resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                JOptionPane.showMessageDialog(null, "Autor não encontrado!");
                return null;
            } else {
                Autor autor = new Autor();
                autor.setIdAutor(resultSet.getInt("id_autor"));
                autor.setNomeCompleto(resultSet.getString("nome_completo"));
                autor.setAreaAtuacao(resultSet.getString("area_atuacao"));
                autor.setEmail(resultSet.getString("email"));
                autor.setTelefone(resultSet.getString("telefone"));

                return autor;
            }
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void insert(Autor entidade) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        try {
            connection = DbUtils.getConnection();

            String sql = "INSERT INTO AUTOR (area_atuacao, email, telefone, nome_completo) VALUES (?, ?, ?, ?)";

            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            
            statement.setString(1, entidade.getAreaAtuacao());
            statement.setString(2, entidade.getEmail());
            statement.setString(3, entidade.getTelefone());
            statement.setString(4, entidade.getNomeCompleto());

            statement.execute();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void update(Autor entidade) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        try {
            connection = DbUtils.getConnection();

            String sql = "UPDATE AUTOR SET area_atuacao=?, email=?, telefone=?, nome_completo=? WHERE id_autor = ?";

            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            statement.setString(1, entidade.getAreaAtuacao());
            statement.setString(2, entidade.getEmail());
            statement.setString(3, entidade.getTelefone());
            statement.setString(4, entidade.getNomeCompleto());
            statement.setInt(5, entidade.getIdAutor());

            statement.execute();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void delete(Autor entidade) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        try {
            connection = DbUtils.getConnection();
            String sql = "DELETE FROM AUTOR WHERE id_autor=?";
            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            statement.setLong(1, entidade.getIdAutor());

            statement.execute();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        
    }
    
}
