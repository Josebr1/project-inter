package br.com.cru.dao;

import br.com.cru.interfaces.IAbstractDao;
import br.com.cru.model.TipoRecurso;
import br.com.cru.utils.DbUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TipoRecursoDAO implements IAbstractDao<TipoRecurso>{

    private Connection connection = null;
    
    @Override
    public List<TipoRecurso> all() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        List<TipoRecurso> listaRecursos = new ArrayList<>();
        ResultSet rs;
        
        try {
            connection = DbUtils.getConnection();
            
            String sql = "SELECT * FROM TIPORECURSO";
            
            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            
            rs = statement.executeQuery();
            
            while (rs.next()) {
                listaRecursos.add(new TipoRecurso(rs.getInt("id_tipo"), rs.getString("nome")));
            }
            
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
        return listaRecursos;
    }

    @Override
    public TipoRecurso findById(long id) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void insert(TipoRecurso entidade) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        try {
            connection = DbUtils.getConnection();
            
            String sql = "INSERT INTO TIPORECURSO (nome) VALUES (?)";
            
            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            statement.setString(1, entidade.getNome());
            
            statement.execute();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void update(TipoRecurso entidade) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        try {
            connection = DbUtils.getConnection();
            String sql = "UPDATE TIPORECURSO SET nome=? WHERE id_tipo = ?";
            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            statement.setString(1, entidade.getNome());
            statement.setLong(2, entidade.getiDTipo());

            statement.execute();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void delete(TipoRecurso entidade) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        try {
            connection = DbUtils.getConnection();
            String sql = "DELETE FROM TIPORECURSO WHERE id_tipo=?";
            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            statement.setLong(1, entidade.getiDTipo());

            statement.execute();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
    
}
