package br.com.cru.dao;

import br.com.cru.interfaces.IAbstractDao;

import br.com.cru.model.RecursoDidatico;
import br.com.cru.utils.DbUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import javax.swing.JOptionPane;


public class RecursoDidaticoDAO implements IAbstractDao<RecursoDidatico>{

    private Connection connection = null;
    
    @Override
    public List<RecursoDidatico> all() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public RecursoDidatico findById(long id) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        ResultSet resultSet;

        try {
            connection = DbUtils.getConnection();
            String sql = "SELECT * FROM RecursoDidatico WHWRE id_rd = ?";
            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);

            statement.setLong(1, id);

            resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                JOptionPane.showMessageDialog(null, "Autor não encontrado!");
                return null;
            } else {
                RecursoDidatico rd = new RecursoDidatico();
                rd.setiDRD(resultSet.getInt("id_rd"));
                rd.setDataCriacao(resultSet.getString("data_criacao"));
                rd.setPalavraChave(resultSet.getString("palavra_chave"));
                rd.setTitulo(resultSet.getString("titulo"));
                rd.setDescConteudo(resultSet.getString("desc_conteudo"));
                rd.setNivelEnsino(resultSet.getString("nivel_ensino"));
                rd.setDescTipoRecurso(resultSet.getString("desc_tipo_recurso"));
                rd.setOrigem(resultSet.getString("origem"));
                rd.setLocalArmazenamento(resultSet.getString("local_armazenamento"));
                rd.setArquivo(resultSet.getString("arquivo"));
                rd.setIdTipo(resultSet.getInt("id_tipo"));
                return rd;
            }
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void insert(RecursoDidatico entidade) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        try {
            connection = DbUtils.getConnection();

            String sql = "INSERT INTO RECURSODIDATICO \n" +
"(titulo, desc_conteudo, nivel_ensino, desc_tipo_recurso, origem, local_armazenamento, arquivo, id_tipo, palavra_chave) \n" +
"VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";

            PreparedStatement statement = DbUtils.getPreparedStatement(connection, sql);
            statement.setString(1, entidade.getTitulo());
            statement.setString(2, entidade.getDescConteudo());
            statement.setString(3, entidade.getNivelEnsino());
            statement.setString(4, entidade.getDescTipoRecurso());
            statement.setString(5, entidade.getOrigem());
            statement.setString(6, entidade.getLocalArmazenamento());
            statement.setString(7, entidade.getArquivo());
            statement.setInt(8, entidade.getIdTipo());
            statement.setString(9, entidade.getPalavraChave());

            statement.execute();
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    @Override
    public void update(RecursoDidatico entidade) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void delete(RecursoDidatico entidade) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

  
    
}
