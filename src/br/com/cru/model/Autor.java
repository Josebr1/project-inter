package br.com.cru.model;

public class Autor {
    
    private int idAutor;
    private String nomeCompleto;
    private String areaAtuacao;
    private String email;
    private String telefone;

    public Autor(int idAutor, String nomeCompleto, String areaAtuacao, String email, String telefone) {
        this.idAutor = idAutor;
        this.nomeCompleto = nomeCompleto;
        this.areaAtuacao = areaAtuacao;
        this.email = email;
        this.telefone = telefone;
    }

    public Autor() {}
    
    public int getIdAutor() {
        return idAutor;
    }

    public void setIdAutor(int idAutor) {
        this.idAutor = idAutor;
    }

    public String getNomeCompleto() {
        return nomeCompleto;
    }

    public void setNomeCompleto(String nomeCompleto) {
        this.nomeCompleto = nomeCompleto;
    }

    public String getAreaAtuacao() {
        return areaAtuacao;
    }

    public void setAreaAtuacao(String areaAtuacao) {
        this.areaAtuacao = areaAtuacao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }
}
