package br.com.cru.model;

public class RecursoDidatico {
    
    private int iDRD;
    private String dataCriacao;
    private String titulo;
    private int iDAutor;
    private String descConteudo;
    private String nivelEnsino;
    private String descTipoRecurso;
    private String palavraChave;
    private String origem;
    private String localArmazenamento;
    private String arquivo;
    private int idTipo;

    public int getiDRD() {
        return iDRD;
    }

    public void setiDRD(int iDRD) {
        this.iDRD = iDRD;
    }

    public String getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(String dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getDescConteudo() {
        return descConteudo;
    }

    public void setDescConteudo(String descConteudo) {
        this.descConteudo = descConteudo;
    }

    public String getNivelEnsino() {
        return nivelEnsino;
    }

    public void setNivelEnsino(String nivelEnsino) {
        this.nivelEnsino = nivelEnsino;
    }

    public String getDescTipoRecurso() {
        return descTipoRecurso;
    }

    public void setDescTipoRecurso(String descTipoRecurso) {
        this.descTipoRecurso = descTipoRecurso;
    }

    public String getOrigem() {
        return origem;
    }

    public void setOrigem(String origem) {
        this.origem = origem;
    }

    public String getLocalArmazenamento() {
        return localArmazenamento;
    }

    public void setLocalArmazenamento(String localArmazenamento) {
        this.localArmazenamento = localArmazenamento;
    }

    public String getArquivo() {
        return arquivo;
    }

    public void setArquivo(String arquivo) {
        this.arquivo = arquivo;
    }

    public int getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(int idTipo) {
        this.idTipo = idTipo;
    }

    public int getiDAutor() {
        return iDAutor;
    }

    public void setiDAutor(int iDAutor) {
        this.iDAutor = iDAutor;
    }

    public String getPalavraChave() {
        return palavraChave;
    }

    public void setPalavraChave(String palavraChave) {
        this.palavraChave = palavraChave;
    }
    
    
}
