package br.com.cru.model;

public class TipoRecurso {

    private int iDTipo;
    private String nome;

    public TipoRecurso(int iDTipo, String nome) {
        this.iDTipo = iDTipo;
        this.nome = nome;
    }
    
    public TipoRecurso() {}
    
    public int getiDTipo() {
        return iDTipo;
    }

    public void setiDTipo(int iDTipo) {
        this.iDTipo = iDTipo;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
    
}
