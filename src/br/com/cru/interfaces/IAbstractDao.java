package br.com.cru.interfaces;

import java.sql.SQLException;
import java.util.List;

public interface IAbstractDao<T> {

    List<T> all() throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException;

    T findById(long id) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException;

    void insert(T entidade) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException;

    void update(T entidade) throws SQLException, InstantiationException, IllegalAccessException, ClassNotFoundException;

    void delete(T entidade) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException;
}