CREATE DATABASE PROJECT_INTER;

USE PROJECT_INTER;

CREATE TABLE RecursoDidatico (
id_rd INT PRIMARY KEY AUTO_INCREMENT,
data_criacao DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
palavra_chave VARCHAR(255) NOT NULL,
titulo VARCHAR(255) NOT NULL,
desc_conteudo VARCHAR(255) NOT NULL,
nivel_ensino ENUM('Infantil', 'Fundamental 1', 'Fundamental 2', 'Médio' , 'Superior') NOT NULL,
desc_tipo_recurso VARCHAR(255) NOT NULL,
origem VARCHAR(255),
local_armazenamento VARCHAR(255) NOT NULL,
arquivo VARCHAR(255) NOT NULL,
id_tipo INT NOT NULL
);

CREATE TABLE Autor (
id_autor INT PRIMARY KEY AUTO_INCREMENT,
area_atuacao VARCHAR(255) NOT NULL,
email VARCHAR(100),
telefone VARCHAR(14),
nome_completo VARCHAR(100) NOT NULL
);

CREATE TABLE TipoRecurso (
id_tipo int PRIMARY KEY AUTO_INCREMENT,
nome VARCHAR(255) UNIQUE NOT NULL
);

select * from TipoRecurso;

CREATE TABLE Atualizado (
id_atualizacao INT PRIMARY KEY AUTO_INCREMENT,
id_rd INT NOT NULL,
id_autor INT NOT NULL,
descricao VARCHAR(255) NOT NULL,
data_atualizacao DATETIME default NOW() NOT NULL,
FOREIGN KEY(id_rd) REFERENCES RecursoDidatico (id_rd),
FOREIGN KEY(id_autor) REFERENCES Autor (id_autor)
);

ALTER TABLE RecursoDidatico ADD FOREIGN KEY(id_tipo) REFERENCES TipoRecurso (id_tipo);